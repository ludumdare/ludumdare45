extends Node2D


onready var res_dialog_panel = preload("res://game/menu/dialog_panel.tscn")


var active: bool = false
var dialog_panel: DialogPanel


func _on_dialog_exit():
	active = false
	dialog_panel.queue_free()
	ECS.clean()
	TransitionManager.transition_to("res://game/menu/welcome.tscn", 2.0)


func _process(delta: float) -> void:

	if active:
		ECS.update()


func _ready() -> void:

	dialog_panel = res_dialog_panel.instance()
	add_child(dialog_panel)
	dialog_panel.get_component("dialog").state = Dialog.DIALOG_STATE_INIT
	dialog_panel.get_component("dialog").dialog_filename = "intro"
	dialog_panel.connect("dialog_exit", self, "_on_dialog_exit")
	dialog_panel.rect_position = Vector2(150,50)
	active = true

