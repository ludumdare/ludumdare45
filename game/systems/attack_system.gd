extends System
class_name AttackSystem

func on_process_entity(entity: Entity, delta: float):

	var _attack = entity.get_component("attack") as Attack
	var _velocity = entity.get_component("velocity") as Velocity

	_velocity.direction = (_attack.target - entity.global_position).normalized()
	_velocity.speed_factor = 30.0

	var _distance = entity.global_position.distance_to(_attack.target)

	if _distance < 10:
		entity.global_position = _attack.target
		entity.remove_component("attack")
		_velocity.direction = Vector2.ZERO
		_velocity.speed_factor = 0.0
		entity.get_component("orbit").active = true
