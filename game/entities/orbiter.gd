extends Entity

onready var n0 = get_node("Nothings/Nothing0")
onready var n1 = get_node("Nothings/Nothing1")
onready var n2 = get_node("Nothings/Nothing2")
onready var n3 = get_node("Nothings/Nothing3")
onready var n4 = get_node("Nothings/Nothing4")
onready var n5 = get_node("Nothings/Nothing5")
onready var n6 = get_node("Nothings/Nothing6")


func _ready() -> void:

	Game.orbiter_slots.append($Slots/Slot0)
	Game.orbiter_slots.append($Slots/Slot1)
	Game.orbiter_slots.append($Slots/Slot2)
	Game.orbiter_slots.append($Slots/Slot3)
	Game.orbiter_slots.append($Slots/Slot4)
	Game.orbiter_slots.append($Slots/Slot5)
	Game.orbiter_slots.append($Slots/Slot6)

	n0.get_component("orbit").slot = Game.orbiter_slots[0]
	n1.get_component("orbit").slot = Game.orbiter_slots[1]
	n2.get_component("orbit").slot = Game.orbiter_slots[2]
	n3.get_component("orbit").slot = Game.orbiter_slots[3]
	n4.get_component("orbit").slot = Game.orbiter_slots[4]
	n5.get_component("orbit").slot = Game.orbiter_slots[5]
	n6.get_component("orbit").slot = Game.orbiter_slots[6]

	n0.get_parent().remove_child(n0)
	n1.get_parent().remove_child(n1)
	n2.get_parent().remove_child(n2)
	n3.get_parent().remove_child(n3)
	n4.get_parent().remove_child(n4)
	n5.get_parent().remove_child(n5)
	n6.get_parent().remove_child(n6)

	Game.add_child(n0)
	Game.add_child(n1)
	Game.add_child(n2)
	Game.add_child(n3)
	Game.add_child(n4)
	Game.add_child(n5)
	Game.add_child(n6)

	Game.nothings.append(n0)
	Game.nothings.append(n1)
	Game.nothings.append(n2)
	Game.nothings.append(n3)
	Game.nothings.append(n4)
	Game.nothings.append(n5)
	Game.nothings.append(n6)
