extends Node

onready var player = $Player
onready var move_to = $MoveTo

func _process(delta: float) -> void:

	ECS.update()

	move_to.global_position = player.get_component("grid").snap_to

