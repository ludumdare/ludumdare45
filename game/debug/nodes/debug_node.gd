extends Node2D
class_name DebugNode

var debug: Debug
var entity: Entity
var text: Array

var DEBUG_COLOR_TEXT = ColorN("lightblue")
var DEBUG_COLOR_LINE_1 = ColorN("green")
var DEBUG_COLOR_LINE_2 = ColorN("red")
var DEBUG_COLOR_LINE_3 = ColorN("yellow")
var DEBUG_COLOR_LINE_4 = ColorN("purple")


func on_draw() -> void:
	draw_text(entity.position + Vector2(-20, -20), [entity.name, str(entity.get_instance_id())], DEBUG_COLOR_LINE_1)


func _draw() -> void:
	on_draw()


func draw_circle_arc(center, radius, angle_from, angle_to, color):
    var nb_points = 32
    var points_arc = PoolVector2Array()

    for i in range(nb_points + 1):
        var angle_point = deg2rad(angle_from + i * (angle_to-angle_from) / nb_points - 90)
        points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)

    for index_point in range(nb_points):
        draw_line(points_arc[index_point], points_arc[index_point + 1], color)


func draw_circle_arc_poly(center, radius, angle_from, angle_to, color):
    var nb_points = 32
    var points_arc = PoolVector2Array()
    points_arc.push_back(center)
    var colors = PoolColorArray([color])

    for i in range(nb_points + 1):
        var angle_point = deg2rad(angle_from + i * (angle_to - angle_from) / nb_points - 90)
        points_arc.push_back(center + Vector2(cos(angle_point), sin(angle_point)) * radius)
    draw_polygon(points_arc, colors)


func draw_text(pos, text, color):

	var y = 0
	for i in range(text.size()):
		draw_string(Game.DebugFontSmall, pos + Vector2(15,y), text[i], DEBUG_COLOR_TEXT)
		y += Game.DebugFontSmall.get_height() + 1
