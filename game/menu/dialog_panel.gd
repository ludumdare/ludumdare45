extends Entity
class_name DialogPanel


signal dialog_start
signal dialog_end
signal dialog_exit

onready var speaker_anim = $SpeakerAnim
onready var speaker_image = $SpeakerImage
onready var speaker_name = $SpeakerName
onready var dialog_text = $DialogText
onready var dialog_button_ok = $DialogButtonOk
onready var dialog_button_next = $DialogButtonNext


func _on_DialogButtonOk_pressed() -> void:
	print("ok")
	get_component("dialog").state = Dialog.DIALOG_STATE_STEP_END


func _on_DialogButtonNext_pressed() -> void:
	print("next")
	get_component("dialog").state = Dialog.DIALOG_STATE_STEP_NEXT
