extends System
class_name GridSnapSystem

const SIZE = 5


func on_process_entity(entity: Entity, delta: float):

	var _moves = entity.get_component("moves") as Moves
	var _grid = entity.get_component("grid") as Grid
	var _velocity = entity.get_component("velocity") as Velocity

	if _moves.is_moving:
		return

	# calculate grid pos
	var gx = int(entity.global_position.x)
	var gy = int(entity.global_position.y)
	entity.global_position = Vector2(gx, gy)
#	Logger.debug(str(_velocity.direction.normalized()))
#	if _velocity.direction.normalized().y != 0:
	if true:
		var diffx = int(entity.global_position.x) % SIZE
		var halfwayx = 9/2
		if diffx > halfwayx:
			entity.global_position.x += (SIZE - diffx)
		else:
			entity.global_position.x += -diffx

#	if _velocity.direction.normalized().x != 0:
	if true:
		var diffy = int(entity.global_position.y) % SIZE
		var halfwayy = 9/2
		if diffy > halfwayy:
			entity.global_position.y += (SIZE - diffy)
		else:
			entity.global_position.y += -diffy


#	Logger.debug(str(entity.global_position))