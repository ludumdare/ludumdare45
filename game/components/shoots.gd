extends Component
class_name Shoots


export var COOL_DOWN: float = 1.5


var cool_down: float = 0.0
var cool_down_timer: Timer
var waiting: bool = false

func _exit_tree() -> void:
	cool_down_timer.free()

func _on_timeout():
	waiting = false
	cool_down_timer.get_parent().remove_child(cool_down_timer)


func _ready() -> void:

	if COOL_DOWN:	cool_down = COOL_DOWN

	cool_down_timer = Timer.new()
	cool_down_timer.wait_time = cool_down

	cool_down_timer.connect("timeout", self,"_on_timeout")

