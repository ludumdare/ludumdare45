extends Component
class_name Rotates

export var ROTATION_SPEED: int = 10
export var ROTATION_SPEED_FACTOR: float = 1.5

var rotation_speed: int = 0
var rotation_speed_factor: float = 0.0

func _ready() -> void:

	if ROTATION_SPEED:			rotation_speed = ROTATION_SPEED
	if ROTATION_SPEED_FACTOR:	rotation_speed_factor = rotation_speed_factor

