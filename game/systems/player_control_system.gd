extends System
class_name PlayerControlSystem

const IDLE_SPEED = 4.0
const MOVE_SPEED = 14.0

func on_process_entity(entity, delta):

	var _entity = entity as Entity
	var _player = _entity.get_component("player") as Player
	var _animated = _entity.get_component("animated") as Animated
	var _velocity = _entity.get_component("velocity") as Velocity
	var _moves = _entity.get_component("moves") as Moves

	var _anim = _player.PLAYER_ANIM_IDLE
	var _dir = Vector2(0, 0)
	var _speed = IDLE_SPEED

	_moves.is_moving = false
	_player.facing_up = false

	Logger.fine("player_control_system")

	if Input.is_action_pressed("game_right"):
		_moves.is_moving = true
		_speed = MOVE_SPEED
		_anim = _player.PLAYER_ANIM_RIGHT
		_player.facing_left = false
		_dir = Vector2(_player.hspeed, 0)

	if Input.is_action_pressed("game_left"):
		_moves.is_moving = true
		_speed = MOVE_SPEED
		_anim = _player.PLAYER_ANIM_LEFT
		_player.facing_left = true
		_dir = Vector2(-_player.hspeed, 0)

	if Input.is_action_pressed("game_up"):
		_moves.is_moving = true
		_speed = MOVE_SPEED
		_anim = _player.PLAYER_ANIM_RIGHT
		_player.facing_up = true
		if _player.facing_left:
			_anim = _player.PLAYER_ANIM_LEFT
		_dir = _dir + Vector2(0, -_player.vspeed)

	if Input.is_action_pressed("game_down"):
		_moves.is_moving = true
		_speed = MOVE_SPEED
		_anim = _player.PLAYER_ANIM_RIGHT
		_player.facing_up = false
		if _player.facing_left:
			_anim = _player.PLAYER_ANIM_LEFT
		_dir = _dir + Vector2(0, _player.vspeed)

	if _player.facing_up:
		_anim += "_up"

	_animated.animation = _anim
	_velocity.direction = _dir
	_animated.animation_speed = _speed
