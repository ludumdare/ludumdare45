extends System
class_name CameraZoneSystem

func on_process_entity(entity, delta):

	var _entity = entity as Entity
	var _zone = _entity.get_component("zone") as Zone
	var _moves = _entity.get_component("moves") as Moves
	var _velocity = _entity.get_component("velocity") as Velocity

	if _zone.location != Game.zone:
		_zone.location = Game.zone
		_zone.next_position = _zone.zone_offset + _zone.location * _zone.zone_size
		_moves.is_moving = true

	if !_moves.is_moving:
		return

	_moves.is_moving = false
	_velocity.direction = Vector2.ZERO

	var _dist = int(entity.global_position.distance_to(_zone.next_position))

#	Logger.trace(str(_dist))

	if _dist > 10:
		_moves.is_moving = true
		var _dir = _zone.next_position - _entity.global_position
		_velocity.direction = _dir.normalized()
#		Logger.trace(str(_velocity.direction))
	else:
		_entity.global_position = _zone.next_position
