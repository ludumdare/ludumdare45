extends Component
class_name Player


export var VERTICAL_SPEED: float = 1.0
export var HORIZONTAL_SPEED: float = 1.0


const PLAYER_ANIM_IDLE = "idle"
const PLAYER_ANIM_LEFT = "left"
const PLAYER_ANIM_RIGHT = "right"


var facing_up: bool = false
var facing_left: bool = false
var vspeed: float = 1.0
var hspeed: float = 1.0

func _ready() -> void:

	if VERTICAL_SPEED:		vspeed = VERTICAL_SPEED
	if HORIZONTAL_SPEED:	hspeed = HORIZONTAL_SPEED