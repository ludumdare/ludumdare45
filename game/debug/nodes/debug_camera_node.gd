extends DebugNode
class_name DebugCameraNode

func on_draw() -> void:

	var _moves = entity.get_component("moves") as Moves
	var _zone = entity.get_component("zone") as Zone

	var _debug = []

	_debug.append("camera [{0}]".format([entity._get_id()]))
	_debug.append("location: {0}".format([_zone.location]))
	_debug.append("next_location: {0}".format([_zone.next_location]))
	_debug.append("is_moving: {0}".format([_moves.is_moving]))
	_debug.append("position: {0}".format([entity.position]))
	_debug.append("next_position: {0}".format([_zone.next_position]))

	draw_text(position + Vector2(-10,10), _debug, DEBUG_COLOR_LINE_1)

