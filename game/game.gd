extends Node


onready var DebugFontSmall: Font = preload("res://game/fonts/debug_8.tres")
onready var DebugFontNormal: Font = preload("res://game/fonts/debug_16.tres")


export var GRID_SIZE: Vector2 = Vector2(20, 20)
export (Resource) var WINDOW_SCALER: Resource

var grid_size: Vector2 = Vector2.ZERO
var zone: Vector2 = Vector2(0,0)

# the slots on the orbiter
var orbiter_slots = []
var nothings = []

func get_available_nothing():
	for n in nothings:
		if n.get_component("nothing").available:
			return n

func _ready() -> void:

	if WINDOW_SCALER:
		WindowScalar.load(WINDOW_SCALER)

	if GRID_SIZE:		grid_size = GRID_SIZE



