extends Component
class_name Velocity

export var SPEED: int = 10
export var SPEED_FACTOR: float = 1.5

var speed: int = 0
var speed_factor: float = 0.0
var direction: Vector2 = Vector2.ZERO
var velocity: Vector2 = Vector2.ZERO

func _ready() -> void:

	if SPEED:			speed = SPEED
	if SPEED_FACTOR:	speed_factor = SPEED_FACTOR
