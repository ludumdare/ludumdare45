extends CanvasLayer


var _scene_to: String
var _animation_speed: float = 1.0


func transition_to(scene: String, speed = 1.0) -> void:

	_scene_to = scene
	_animation_speed = 1.0 / speed

	$FadeAnimation.play("fade_out", -1, _animation_speed)


func _on_FadeAnimation_animation_finished(anim_name: String) -> void:

	if (anim_name == "fade_out"):

		var _results = get_tree().change_scene(_scene_to)

		if _results != OK:

			print("error changing scene")
			return

		$FadeAnimation.play("fade_in", -1, _animation_speed)

