extends System
class_name PlayerShootSystem


func on_process(entities, delta):

	for entity in entities:

		var _entity = entity as Entity
		var _player = _entity.get_component("player") as Player
		var _shoots = _entity.get_component("shoots") as Shoots



		Logger.fine("player_shoots_system")

		if Input.is_action_just_pressed("game_fire"):

			var _nothing = Game.get_available_nothing()

			if _nothing:
					_nothing.get_component("orbit").active = false
					_nothing.get_component("nothing").available = false
					var attack = Attack.new()
					attack.name = "attack"
					attack.target = entity.get_global_mouse_position()
					_nothing.add_component(attack)

#			if _shoots.waiting:
#				Logger.debug("waiting")
#				return
#
#			_shoots.waiting = true
#			_entity.add_child(_shoots.cool_down_timer)
#			_shoots.cool_down_timer.start()
#			Logger.debug("fire")
