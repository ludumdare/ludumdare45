extends DebugNode
class_name DebugCollidersNode

func on_draw() -> void:

	var _collides = entity.get_component("collides") as Collides

	var _debug = []

	_debug.append("has_collides: {0}".format([_collides.has_collided]))
	_debug.append("collider_name: {0}".format([_collides.collider_name]))

	draw_text(entity.position + Vector2(-20, 20), _debug, DEBUG_COLOR_LINE_1)
