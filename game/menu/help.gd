extends TextureRect

export var HELP_RESOURCE: String = "res://game/data/help.txt"

func _ready() -> void:

	var _file = File.new()

	if (_file.file_exists(HELP_RESOURCE)):

		_file.open(HELP_RESOURCE, File.READ)
		$Panel/HelpText.bbcode_text = _file.get_as_text()
		_file.close()