extends System
class_name ScanSystem

func on_process_entity(entity: Entity, delta: float):

	var _velocity = entity.get_component("velocity") as Velocity
	var _scan = entity.get_component("scan") as Scan

	var _dir = _velocity.direction
	var _result

	# cast rays up
	if _dir.y < 0:
		_result = _cast_rays(entity, Vector2.UP, _scan.rays)
		if not _result.empty():
			_dir.y = 0

	# cast rays down
	if _dir.y > 0:
		_result = _cast_rays(entity, Vector2.DOWN, _scan.rays)
		if not _result.empty():
			_dir.y = 0

	# cast rays left
	if _dir.x < 0:
		_result = _cast_rays(entity, Vector2.LEFT, _scan.rays)
		if not _result.empty():
			_dir.x = 0

	# cast rays right
	if _dir.x > 0:
		_result = _cast_rays(entity, Vector2.RIGHT, _scan.rays)
		if not _result.empty():
			_dir.x = 0

	_velocity.direction = _dir

func _cast_rays(node, direction: Vector2, count: int = 1):
	var _space_state = node.get_world_2d().get_direct_space_state() as Physics2DDirectSpaceState
	var _spacing = int(20 / count)
	var _result
	var _pos

	# good enough for now

	_pos = node.global_position
	return _space_state.intersect_ray(_pos, _pos + (direction * 10), [node])

	# multiple scanning if I have time

	for i in range(count):

		if direction == Vector2.UP:
			_pos = node.global_position - Vector2(-10, 0) + Vector2(_spacing, 0)
			_result = _space_state.intersect_ray(_pos, _pos + (direction * 10), [node])
			if not _result.empty():
				return _result

		if direction == Vector2.DOWN:
			_pos = node.global_position - Vector2(-10, 0) + Vector2(_spacing, 0)
			_result = _space_state.intersect_ray(_pos, _pos + (direction * 10), [node])
			if not _result.empty():
				return _result

		if direction == Vector2.LEFT:
			_pos = node.global_position - Vector2(0, -10) + Vector2(_spacing, 0)
			_result = _space_state.intersect_ray(_pos, _pos + (direction * 10), [node])
			if not _result.empty():
				return _result

		if direction == Vector2.RIGHT:
			_pos = node.global_position - Vector2(0, -10) + Vector2(_spacing, 0)
			_result = _space_state.intersect_ray(_pos, _pos + (direction * 10), [node])
			if not _result.empty():
				return _result


	return {}
