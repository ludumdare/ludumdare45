extends Component
class_name Zone


export var start_location: Vector2 = Vector2.ZERO
export var zone_size: Vector2 = Vector2(480, 220)
export var zone_offset: Vector2 = Vector2(0, -50)


var location: Vector2 = Vector2.ZERO
var next_location: Vector2 = Vector2.ZERO
var next_position: Vector2 = next_location

func _ready() -> void:

	if start_location:	location = start_location