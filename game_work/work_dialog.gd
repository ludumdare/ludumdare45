extends Node2D


onready var res_dialog_panel = preload("res://game/menu/dialog_panel.tscn")


var active: bool = false
var dialog_panel: DialogPanel


func _on_dialog_exit():
	active = false
	dialog_panel.queue_free()

func _process(delta: float) -> void:

	if active:
		ECS.update()


func _ready() -> void:

	dialog_panel = res_dialog_panel.instance()
	add_child(dialog_panel)
	dialog_panel.get_component("dialog").state = Dialog.DIALOG_STATE_INIT
	dialog_panel.get_component("dialog").dialog_filename = "test"
	dialog_panel.connect("dialog_exit", self, "_on_dialog_exit")
	active = true

