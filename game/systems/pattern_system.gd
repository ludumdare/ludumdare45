extends System
class_name PatternSystem


func _step_start(entity: Entity, pattern: Pattern):

	var _velocity = entity.get_component("velocity") as Velocity
	_velocity.direction = _get_direction(pattern.pattern[pattern.pattern_index])
	pattern.frame = 0


func _step_run(entity: Entity, pattern: Pattern):
	pattern.frame += 1
	if (pattern.frame > pattern.pattern_length):
		var _velocity = entity.get_component("velocity") as Velocity
		_velocity.direction = Vector2.ZERO
		pattern.pattern_state = pattern.PATTERN_STEP_DONE


func _get_direction(code: String):

	if (code.to_lower() == "r"): return Vector2.RIGHT
	if (code.to_lower() == "l"): return Vector2.LEFT
	if (code.to_lower() == "u"): return Vector2.UP
	if (code.to_lower() == "d"): return Vector2.DOWN

	return

func on_process_entity(entity: Entity, delta: float) -> void:


	var _pattern = entity.get_component("pattern") as Pattern


	if _pattern.pattern_state == _pattern.PATTERN_INIT:
		randomize()
		Logger.trace(_pattern.pattern_state)
		entity.add_child(_pattern.pattern_timer)
		_pattern.pattern_index = int(rand_range(0, _pattern.pattern.length()-1))
		_pattern.pattern_state = _pattern.PATTERN_START
		return

	if _pattern.pattern_state == _pattern.PATTERN_START:
		Logger.trace(_pattern.pattern_state)
		_pattern.pattern_state = _pattern.PATTERN_STEP_START
		return


	if (_pattern.pattern_state == _pattern.PATTERN_STEP_START):
		Logger.trace(_pattern.pattern_state)
		_step_start(entity, _pattern)
		_pattern.pattern_state = _pattern.PATTERN_STEP_RUN
		return


	if (_pattern.pattern_state == _pattern.PATTERN_STEP_RUN):
		Logger.fine(_pattern.pattern_state)
		_step_run(entity, _pattern)
		return


	if (_pattern.pattern_state == _pattern.PATTERN_STEP_DONE):
		Logger.trace(_pattern.pattern_state)
		_pattern.waiting = true
		_pattern.pattern_timer.start()
		_pattern.pattern_state = _pattern.PATTERN_STEP_WAIT


	if (_pattern.pattern_state == _pattern.PATTERN_STEP_WAIT):
		Logger.fine(_pattern.pattern_state)
		if _pattern.waiting:
			return

		Logger.trace("- done waiting")
		_pattern.pattern_state = _pattern.PATTERN_STEP_NEXT


	if (_pattern.pattern_state == _pattern.PATTERN_STEP_NEXT):
		Logger.trace(_pattern.pattern_state)
		_pattern.pattern_index += 1

		if (_pattern.pattern_index > _pattern.pattern.length() - 1):
			_pattern.pattern_index = 0
			_pattern.pattern_state = _pattern.PATTERN_DONE
			return

		_pattern.pattern_state = _pattern.PATTERN_STEP_START
		return


	if (_pattern.pattern_state == _pattern.PATTERN_DONE):
		Logger.trace(_pattern.pattern_state)
		_pattern.pattern_state = _pattern.PATTERN_START
		return



