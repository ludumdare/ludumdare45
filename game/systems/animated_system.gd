extends System
class_name AnimatedSystem

func on_process(entities, delta):

	for entity in entities:

		var _animated = entity.get_component("animated") as Animated
		var _ap = entity.get_node("AnimationPlayer") as AnimationPlayer

		if _animated.animation != _animated.last_animation:

			_ap.current_animation = _animated.animation
			_ap.playback_speed = _animated.animation_speed
			_animated.last_animation = _animated.animation

