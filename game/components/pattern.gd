extends Component
class_name Pattern


export var PATTERN: String = "urdl"
export var PATTERN_DELAY: float = 1.0
export var PATTERN_LENGTH: int = 10


const PATTERN_INIT = "pattern_init"
const PATTERN_START = "pattern_start"
const PATTERN_STEP_START = "pattern_step_start"
const PATTERN_STEP_RUN = "pattern_step_run"
const PATTERN_STEP_DONE = "pattern_step_done"
const PATTERN_STEP_WAIT = "pattern_step_wait"
const PATTERN_STEP_NEXT = "pattern_step_next"
const PATTERN_DONE = "pattern_done"


# the whole pattern string
var pattern: String

# how long to delay between steps
var pattern_delay: float

# what index are we running
var pattern_index: int

# state of the pattern
var pattern_state: String

# timer for the pattern
var pattern_timer: Timer

# are we waiting?
var waiting: bool

# the length of each step in frames
var pattern_length: int
var frame: int


func _exit_tree() -> void:
	pattern_timer.queue_free()

func _on_timeout():
	waiting = false

func _ready() -> void:

	if PATTERN:			pattern = PATTERN
	if PATTERN_DELAY:	pattern_delay = PATTERN_DELAY
	if PATTERN_LENGTH:	pattern_length = PATTERN_LENGTH

	pattern_timer = Timer.new()
	pattern_timer.wait_time = pattern_delay + rand_range(-0.5, 0.6)

	pattern_timer.connect("timeout", self, "_on_timeout")

	pattern_state = PATTERN_INIT
