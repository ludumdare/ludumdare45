extends System
class_name EdgeSystem

func on_process_entity(entity: Entity, delta):

	var _edge = entity.get_component("edge") as Edge

	# calculate zone

	var zx = int(entity.global_position.x  / 480.0 )
	var zy = int(entity.global_position.y  / 220.0 )

	_edge.zone = Vector2(zx, zy)
	Game.zone = _edge.zone

