extends DebugNode

func on_draw() -> void:

	var _edge = entity.get_component("edge") as Edge

	var _debug = []
	_debug.append("zone: {0}".format([_edge.zone]))

	draw_text(entity.position + Vector2(-20, -20), _debug, DEBUG_COLOR_LINE_1)

