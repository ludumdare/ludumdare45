extends System

onready var zone = $Camera/Components/Zone as Zone
onready var moves = $Camera/Components/Moves


func _process(delta: float) -> void:

	ECS.update("systems")


func _physics_process(delta: float) -> void:

	ECS.update("physicssystems")

	if moves.is_moving:
		return

	if Input.is_action_just_pressed("game_left"):
		Game.zone += Vector2.LEFT

	if Input.is_action_just_pressed("game_right"):
		Game.zone += Vector2.RIGHT

	if Input.is_action_just_pressed("game_up"):
		Game.zone += Vector2.UP

	if Input.is_action_just_pressed("game_down"):
		Game.zone += Vector2.DOWN


