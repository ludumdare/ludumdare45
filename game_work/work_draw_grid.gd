tool
extends Node2D

export var grid_width: int = 4
export var grid_depth: int = 4
export var grid_size: Vector2 = Vector2(960, 540)
export (Color) var grid_color = ColorN("blue")
export var show_in_game: bool = false


func _draw() -> void:

	if !Engine.editor_hint:
		if !show_in_game:
			return

	for x in grid_width:

		for y in grid_depth:

			var top_l = Vector2(grid_size.x * x, grid_size.y * y)
			var top_r = Vector2(top_l.x + grid_size.x, grid_size.y * y)
			var bot_l = Vector2(top_l.x, top_r.y + grid_size.y)
			var bot_r = Vector2(top_r.x, bot_l.y)

			draw_line(top_l, top_r, grid_color, 5.0, false)
			draw_line(top_l, bot_l, grid_color, 5.0, false)
			draw_line(bot_l, bot_r, grid_color, 5.0, false)
			draw_line(bot_r, top_r, grid_color, 5.0, false)


func _process(delta: float) -> void:
	update()
