extends System
class_name AimSystem


func on_process_entity(entity: Entity, delta: float):

	var _animated = entity.get_component("animated") as Animated

	entity.global_position = entity.get_global_mouse_position()

	var _objs = entity.get_overlapping_areas()
	var _anim = "default"
	var _speed = 0.0

	if _objs.size() > 0:

		var _obj = _objs[0]

		if _obj.name.to_lower().find("alien") >= 0:
			_anim = "attack"
			_speed = 2.0

		if _obj.name.to_lower().find("survivor") >= 0:
			_anim = "action"
			_speed = 6.0

	_animated.animation = _anim
	_animated.animation_speed = _speed

