extends System
class_name OrbitSystem

func on_process_entity(entity: Entity, delta: float):

	var _orbit = entity.get_component("orbit") as Orbit
	var _velocity = entity.get_component("velocity") as Velocity
	var _nothing = entity.get_component("nothing") as Nothing

	if !_orbit.active:
		return

	if !_orbit.slot:
		return

	_velocity.direction = (_orbit.slot.global_position - entity.global_position).normalized()
	_velocity.speed_factor = 15.0
	_nothing.available = false

	var _distance = entity.global_position.distance_to(_orbit.slot.global_position)

	if _distance < 25:
		_velocity.speed_factor = 10.0
		_nothing.available = true

	if _distance < 10:
		_velocity.speed_factor = 5.0

	if _distance < 5:
		_velocity.speed_factor = 2.5

	if _distance < 3:
		_velocity.speed_factor = 0.5
