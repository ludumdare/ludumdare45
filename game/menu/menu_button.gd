extends TextureButton


export var NEXT_SCENE: String = "home"
export var TRANSITION_SPEED: float = 2.0
export var LABEL = "play"


func _ready() -> void:
	$Label.text = LABEL

func _on_MenuButton_pressed() -> void:

	if NEXT_SCENE:
		ECS.clean()
		TransitionManager.transition_to(NEXT_SCENE)
