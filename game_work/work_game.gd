extends Node


func _physics_process(delta: float) -> void:
	ECS.update("fixedsystems")

func _process(delta: float) -> void:
	ECS.update("systems")