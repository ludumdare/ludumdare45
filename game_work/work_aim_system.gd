extends Node


func _physics_process(delta: float) -> void:
	ECS.update()


func _ready() -> void:
	Input.set_mouse_mode(Input.MOUSE_MODE_HIDDEN)

