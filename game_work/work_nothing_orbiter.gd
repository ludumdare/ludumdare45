extends Node


onready var target = $Target


func _physics_process(delta: float) -> void:
	ECS.update()


func _on_Shoot_pressed() -> void:
	var _nothing = Game.get_available_nothing()
	if _nothing:
		_nothing.get_component("orbit").active = false
		_nothing.get_component("nothing").available = false
		var attack = Attack.new()
		attack.name = "attack"
		attack.target = target.global_position
		_nothing.add_component(attack)


func _on_Return_pressed() -> void:
	pass
#	Game.nothings[0].get_component("orbit").active = false

