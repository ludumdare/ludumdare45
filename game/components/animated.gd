extends Component
class_name Animated


export var ANIMATION: String = "none"


var animation: String = ""
var animation_speed: float = 12.0
var last_animation: String = ""


func _ready() -> void:
	if ANIMATION:	animation = ANIMATION