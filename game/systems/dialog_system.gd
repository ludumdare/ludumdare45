extends System

var button
var text
var link
var image

func on_process_entity(entity: Entity, delta: float):

	var _entity = entity as DialogPanel
	var _dialog = entity.get_component("dialog") as Dialog


	# init the dialog
	if (_dialog.state == _dialog.DIALOG_STATE_INIT):
		Logger.trace(_dialog.state)
		_dialog.state = Dialog.DIALOG_STATE_BEGIN


	# begin the dialog
	if (_dialog.state == Dialog.DIALOG_STATE_BEGIN):
		Logger.trace(_dialog.state)
		_dialog.state = Dialog.DIALOG_STATE_STEP_BEGIN
		_entity.dialog_button_ok.visible = false
		_entity.dialog_button_next.visible = false
		_entity.speaker_name.text = ""
		_entity.dialog_text.bbcode_text = ""


	# begin dialog step
	if (_dialog.state == Dialog.DIALOG_STATE_STEP_BEGIN):
		Logger.trace(_dialog.state)
		_dialog.state = Dialog.DIALOG_STATE_STEP_RUN

		var _filename = _dialog.dialog_location +  _dialog.dialog_filename + ".txt"
		var _config = ConfigFile.new()
		var _error = _config.load(_filename)

		if (!_error == OK):
			Logger.warn("- dialog text file {0} not found".format([_filename]))
			_dialog.state = Dialog.DIALOG_EXIT
			return

		if (!_config.has_section(_dialog.dialog_section)):
			Logger.warn("- dialog section {0} not found".format([_dialog.dialog_section]))
			_dialog.state = Dialog.DIALOG_EXIT
			return

		button = _config.get_value(_dialog.dialog_section, "button")
		text = _config.get_value(_dialog.dialog_section, "text")
		link = _config.get_value(_dialog.dialog_section, "link")
		image = _config.get_value(_dialog.dialog_section, "image")

		if button == "ok":
			_entity.dialog_button_ok.visible = true

		if button == "next":
			_entity.dialog_button_next.visible = true

		var _image = "missing"

		if image:

			_entity.speaker_name.text = image
			_image = image

		if text:
			_entity.dialog_text.bbcode_text = text

		_entity.speaker_anim.play(_image)


	# run dialog step
	if (_dialog.state == Dialog.DIALOG_STATE_STEP_RUN):
		Logger.trace(_dialog.state)
		_dialog.state = Dialog.DIALOG_STATE_STEP_WAIT


	# wait for user interaction
	if (_dialog.state == Dialog.DIALOG_STATE_STEP_WAIT):
		Logger.fine(_dialog.state)


	# process next dialog
	if (_dialog.state == Dialog.DIALOG_STATE_STEP_NEXT):
		Logger.trace(_dialog.state)
		_dialog.state = Dialog.DIALOG_STATE_BEGIN
		_dialog.dialog_section = link


	# process end of dialog step
	if (_dialog.state == Dialog.DIALOG_STATE_STEP_END):
		Logger.trace(_dialog.state)
		_dialog.state = Dialog.DIALOG_STATE_END


	# process end of dialog
	if (_dialog.state == Dialog.DIALOG_STATE_END):
		Logger.trace(_dialog.state)
		_dialog.state = Dialog.DIALOG_EXIT
		_entity.emit_signal("dialog_exit")

