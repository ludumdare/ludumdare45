
extends System
class_name CollidesSystem

func on_process_entity(entity: Entity, delta: float):

	var o = entity.get_overlapping_areas()

	entity.get_component("collides").has_collided = false
	entity.get_component("collides").collider_name = ""

	if o.size() > 0:


		entity.get_component("collides").has_collided = true
		entity.get_component("collides").collider_name = o[0].name