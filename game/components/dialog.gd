extends Component
class_name Dialog

export var DIALOG_LOCATION: String = "res://game/data/"
export var DIALOG_FILENAME: String = "dialog"
export var DIALOG_SECTION: String = "default"


const DIALOG_STATE_INIT = "dialog_state_init"
const DIALOG_STATE_BEGIN = "dialog_state_begin"
const DIALOG_STATE_STEP_BEGIN = "dialog_state_step_begin"
const DIALOG_STATE_STEP_RUN = "dialog_state_step_run"
const DIALOG_STATE_STEP_WAIT = "dialog_state_step_wait"
const DIALOG_STATE_STEP_NEXT = "dialog_state_step_next"
const DIALOG_STATE_STEP_END = "dialog_state_step_end"
const DIALOG_STATE_END = "dialog_state_end"
const DIALOG_EXIT = "dialog_exit"


var state: String
var dialog_location: String
var dialog_section: String
var dialog_filename: String

func _ready() -> void:

	if (DIALOG_LOCATION):	dialog_location = DIALOG_LOCATION
	if (DIALOG_SECTION):	dialog_section = DIALOG_SECTION
	if (DIALOG_FILENAME):	dialog_filename = DIALOG_FILENAME
