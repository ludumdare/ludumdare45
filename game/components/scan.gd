extends Component
class_name Scan

export var RAYS: int = 4


# the number of rays to cast per direction
var rays: int = 4


func _ready() -> void:

	if RAYS:		rays = RAYS
