extends DebugNode
class_name DebugPlayerNode

func on_draw() -> void:

	var _animated = entity.get_component("animated") as Animated
	var _velocity = entity.get_component("velocity") as Velocity
	var _moves = entity.get_component("moves") as Moves

	var _debug = []

	_debug.append("animation: {0}".format([_animated.animation]))
	_debug.append("direction: {0}".format([_velocity.direction]))
	_debug.append("velocity: {0}".format([_velocity.velocity]))
	_debug.append("is_moving: {0}".format([_moves.is_moving]))

	draw_text(entity.position + Vector2(-20, 20), _debug, DEBUG_COLOR_LINE_1)

